---
title: Установка на Heroku
layout: default
parent: Установка
nav_order: 3
---

# {{ page.title }}

## Кнопка Deploy

Вы можете нажать на кнопку [![Deploy](https://www.herokucdn.com/deploy/button.svg)](
https://www.heroku.com/deploy/?template=https://github.com/friendly-telegram/gitlab-mirror) для установки на Heroku. 

Просто нажмите фиолетовую кнопку с надписью «Deploy App», а затем «Open App», когда она будет готова.

## Скрипт

Следуйте инструкциям в [автоматической установке](../automated/), но поменяйте использующуюся команду на
```
(. <($(which curl>/dev/null&&echo curl -Ls||echo wget -qO-) https://kutt.it/ftgi) --heroku)
```
