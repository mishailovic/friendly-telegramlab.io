---
title: Docker установка
layout: default
parent: Установка
nav_order: 1
---

# {{ page.title }}

Для хостинга на системах Linux, вы можете использовать Podman или Docker. Просто введите эту команду в shell:

```
podman pull registry.gitlab.com/friendly-telegram/friendly-telegram && podman run -itv "$(pwd)":/data:Z -p ${PORT:-8080}:8080 $(curl -s https://gitlab.com/friendly-telegram/friendly-telegram/-/raw/master/Dockerfile.modules | podman build -qf- -v "$(pwd)":/data:Z)
```

Разбор команд:
 - `podman pull registry.gitlab.com/friendly-telegram/friendly-telegram`
   Скачивает образы из [регистра](https://gitlab.com/friendly-telegram/friendly-telegram/container_registry)
 - `curl -s https://gitlab.com/friendly-telegram/friendly-telegram/-/raw/master/Dockerfile.modules | podman build -qf- -v "$(pwd)":/data:Z`
   Проверяет и обновляет загрузчик дополнительных модулей, запускает его (это устанавливает зависимости для загруженных модулей в образ docker)
 - `podman run -itv "$(pwd)":/data:Z -p ${PORT:-8080}:8080`
   Запускает образ (включение бота)

Если вам надо перезагрузить бота, просто введите команду снова - она автоматически проверит и установит обновления

Если вы не можете использовать podman и предпочитаете использовать docker, замените `podman` на `docker` в команде
