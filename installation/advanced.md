---
title: Ручная установка
layout: default
parent: Установка
nav_order: 4
---

# {{ page.title }}

Страница разделена на методы для различных платформ

## Debian-like Linux

1. Обновите список пакетов
```
sudo apt update
```

2. Установите необходимые пакеты
```
sudo apt install python3 python3-pip git
```

3. Установите дополнительные зависимости для модулей
```
# Pillow для stickers/kang
sudo apt install python3-dev libwebp-dev libz-dev libjpeg-dev libopenjp2-7 libtiff5
# Cairo для анимированных стикеров
sudo apt install libffi-dev libcairo2
# Утилиты
sudo apt install neofetch
# UI
sudo apt install dialog
```

4. Сохраните исходный код
```
cd
git clone https://gitlab.com/friendly-telegram/friendly-telegram
cd friendly-telegram
```

5. Установите зависимости
```
python3 -m pip install -r requirements.txt
```

6. Следуйте инструкциям описанным [здесь](https://core.telegram.org/api/obtaining_api_id "здесь") для получения API key/hash и ID

7. Запустите скрипт конфигурации
```
python3 -m friendly-telegram --setup
```

8. В обновившемся меню выберите "API Key/hash and ID"
Когда запрошено, введите ваш API key/hash и ID

9. Запустите бота:
```
cd ~/friendly-telegram
python3 -m friendly-telegram
```

## Termux

```
pkg install git python libjpeg-turbo zlib libwebp libffi libcairo build-essential dialog neofetch
git clone https://gitlab.com/friendly-telegram/friendly-telegram
cd friendly-telegram
pip install -r requirements.txt
# For setup
python -m friendly-telegram --setup
# And again for actual execution
python -m friendly-telegram
```

## Windows

1. Установите Git с [официального сайта](https://git-scm.com "официального сайта"). **Проверьте, что Git добавлен в PATH**

2. Установите Python с [официального сайта](https://www.python.org/downloads/windows "официального сайта"). **роверьте, что Python добавлен в PATH**

3.Откройте Windows Powershell [способ открыть](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&cad=rja&uact=8&ved=2ahUKEwijicaXspvkAhVDaFAKHT26DHgQFjACegQIChAG&url=https%3A%2F%2Fwww.isunshare.com%2Fwindows-10%2F5-ways-to-open-windows-powershell-in-windows-10.html "способ открыть"). 

4. Следуйте интсрукциям описанным [здесь](https://core.telegram.org/api/obtaining_api_id "здесь") для получения API key/hash и ID

5. Введите:
```
git clone https://gitlab.com/friendly-telegram/friendly-telegram
cd friendly-telegram
python3 -m pip install -r requirements.txt
python3 -m friendly-telegram
```

6. Когда запрошено, введите ваш API key/hash и ID (обратите внимание, что меню в Windows немного археично, прочитайте все, что выводит программа, чтобы понять лучше)

7. Введите 
```
python -m friendly-telegram
```
Еще раз для активации бота

## Mac OS X

1. Установите Homebrew
```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

2. Установите необходимые пакеты
```
brew install python3  git
```

3. Установите дополнительные зависимости для модулей
```
# Utilities
brew install neofetch
# UI
brew install dialog
```

4. Вручную установите необходимые пакеты (необходимо для поодержки стикеров):
 - libwebp -> https://github.com/webmproject/libwebp
 - libjpeg -> https://github.com/LuaDist/libjpeg

5. Следуйте интсрукциям описанным [здесь](https://core.telegram.org/api/obtaining_api_id "здесь") для получения API key/hash и ID

6. Введите:
```
git clone https://gitlab.com/friendly-telegram/friendly-telegram
cd friendly-telegram
python -m pip install -r requirements.txt
python -m friendly-telegram
```

7. Когда запрошено, введите ваш API key/hash и ID

8. Введите 
```
python -m friendly-telegram
```
Еще раз для активации бота
