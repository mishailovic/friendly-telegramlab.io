---
title: Forwards
layout: default
parent: Модули
---

# {{ page.title }}

## Команды

- **Переслать все сообщения чата пользователю**
[Syntax: `fwdall <to_user>`]
  Пересылает все сообщения текущего чата указанному пользователю.

  Пример: `fwdall @JustAnotherUsername`
