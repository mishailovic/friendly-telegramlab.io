---
title: CLI
layout: default
parent: Конфигурация
nav_order: 2
---

# {{ page.title }}

Настроить очень легко! Просто изучите данную документацию.

## Чтобы войти в настройки:

```
python -m friendly-telegram --setup
```

## Управление модулями

Юзербот полностью состоит из модулей, каждый модуль имеет свои настройки, так же модули можно отключить.

Если вы выключите иодуль, он просто не загрузится при инициализации юзербота. Чтобы заново его включить вам нужно изменить конфигурацию, и перезагрузить юзербот.

Настройки модулей хранятся **Внутри вашего аккаунта Telegram.** Найти конфигурацию юзербота можно найти в чате котрый называется `friendly-{ваш id}-data`. **Ни в коем случае не удаляйте его**. Конфигурация содержит такие параметры как ваш AFK статус, и т. д. **Никогда не делитесь этим сообщением ни с кем** потому что в конфигурации хранятся важные персональные данные вроде API токенов. 

Чтобы настроить модули для начала зайдите в конфигурацию (Она обчно находится в localhost или в имени приложения на Heroku) Найдите нужный модуль в списке, и введите нужные значения в полях. Как только закончите нажмите Enter.

Учтите если вы меняете конфиг на одном компьютере, в то время как хостинг происходит на другом, юзербот следует перезагрузить чтобы изменения вступили в силу.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE2NzQ1MDkzNzVdfQ==
-->
